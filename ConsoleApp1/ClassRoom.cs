﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class ClassRoom
    {
        private string classroom;
        private string faculty;
        public List<Student> StudentList { get; set; }
        public string Classroom { get => classroom; set => classroom = value; }
        public string Faculty { get => faculty; set => faculty = value; }

        public ClassRoom()
        {
            StudentList = new List<Student>();
        }
        public void Input()
        {
                try
                {
                    Console.WriteLine("Nhap ten lop: ");
                    classroom = Console.ReadLine();
                    Console.WriteLine("Nhap khoa: ");
                    faculty = Console.ReadLine();
                    do
                    {
                         try
                            {
                             Console.WriteLine("Nhap luong sinh vien can them: ");
                             int N = int.Parse(Console.ReadLine());
                            for (int i = 0; i < N; i++)
                                {
                                 Student std = new Student();
                                 std.Input();
                                 StudentList.Add(std);
                                 }
                             break;
                    }
                         catch (Exception)
                            {
                              Console.WriteLine("Nhap sai dinh dang!!!");
                            }
                    } while (true);
                }
                catch (Exception)
                {

                    Console.WriteLine("Nhap sai dinh dang!!!");
                }
        }

        public void Input1()
        {
            Console.WriteLine("Nhap ten lop: ");
            classroom = Console.ReadLine();
            Console.WriteLine("Nhap khoa: ");
            faculty = Console.ReadLine();
                Student std = new Student();
                std.Input();
                StudentList.Add(std);
        }
        public void DisPlay()
        {
            Console.WriteLine("Ten lop: {0}, Khoa: {1}", classroom, faculty);
            foreach (Student item in StudentList)
            {
                item.Display();
            }
        }
    }
}
