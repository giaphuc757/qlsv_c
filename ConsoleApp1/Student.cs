﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Student
    {
        private string idstudent;
        private string fullname;
        private string birthday;
        private string email;
        private string gender;
        private string address;
        private float score1;
        private float score2;
        private float score3;
        private float score4;
        private float avg;
        public string Idstudent { get => idstudent; set => idstudent = value; }
        public string Fullname { get => fullname; set => fullname = value; }
        public string Birthday { get => birthday; set => birthday = value; }
        public string Email { get => email; set => email = value; }
        public string Gender { get => gender; set => gender = value; }
        public string Address { get => address; set => address = value; }
        public float Score1 { get => score1; set => score1 = value; }
        public float Score2 { get => score2; set => score2 = value; }
        public float Score3 { get => score3; set => score3 = value; }
        public float Score4 { get => score4; set => score4 = value; }
        public float Avg
        {
            get
            {
                return avg = (score1 + score2 + score3 + score4) / 4;
            }
        }
        public Student()
        {

        }
        public void Input()
        {
            try
            {
                Console.WriteLine("Nhap ID: ");
                Idstudent = Console.ReadLine();
                Console.WriteLine("Nhap ten: ");
                Fullname = Console.ReadLine();
                Console.WriteLine("Nhap ngay sinh(dd-mm-yyyy): ");
                Birthday = Console.ReadLine();
                Console.WriteLine("Nhap email: ");
                Email = Console.ReadLine();
                Console.WriteLine("Nhap gioi tinh: ");
                Gender = Console.ReadLine();
                Console.WriteLine("Nhap dia chi: ");
                Address = Console.ReadLine();
                do
                {
                    try
                    {
                        Console.WriteLine("Nhap diem 1: ");
                        Score1 = float.Parse(Console.ReadLine());
                        Console.WriteLine("Nhap diem 2: ");
                        Score2 = float.Parse(Console.ReadLine());
                        Console.WriteLine("Nhap diem 3: ");
                        Score3 = float.Parse(Console.ReadLine());
                        Console.WriteLine("Nhap diem 4: ");
                        Score4 = float.Parse(Console.ReadLine());
                        break;
                    }
                    catch (Exception)
                    {

                        Console.WriteLine("Nhap sai dinh dang: ");
                    }
                } while (true);
            }
            catch (Exception)
            {
                Console.WriteLine("Nhap sai dinh dang!!!!");
            }
        }
        public void Display()
        {
            Console.WriteLine("ID: {0}, Ho va Ten: {1},Ngay sinh: {2},Gioi tinh {3},Email: {4},Dia Chi: {5},Diem 1: {6},Diem 2: {7},Diem 3: {8},Diem 4: {0},Diem trung binh: {10},", Idstudent, Fullname,Birthday,Gender,Email,Address,Score1,Score2,Score3,Score4,Avg);
        }
    }
}
