﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<ClassRoom> classRooms = new List<ClassRoom>();
            int choose;
            do
            {
                try
                {
                    ShowMenu();
                    choose = int.Parse(Console.ReadLine());
                    switch (choose)
                    {
                        case 1:
                            Console.Clear();
                            classRooms = ImportJson();
                            break;
                        case 2:
                            Console.Clear();
                            Display(classRooms);
                            break;
                        case 3:
                            Console.Clear();
                            Console.WriteLine("Them 1 sinh vien");
                            AddStudent(classRooms);
                            Display(classRooms);
                            break;
                        case 4:
                            Console.Clear();
                            Console.WriteLine("Them 1 sinh vien");
                            Input(classRooms);
                            Display(classRooms);
                            break;
                        case 5:
                            Console.Clear();
                            Rank(classRooms);
                            break;
                        case 6:
                            Console.Clear();
                            Console.WriteLine("Sinh vien yeu bi canh bao");
                            WeakStudent(classRooms);
                            break;
                        case 7:
                            Console.Clear();
                            FindStudent(classRooms);
                            break;
                        case 8:
                            Console.Clear();
                            FindStudent1(classRooms);
                            break;
                        case 0:
                            break;
                        default:
                            Console.WriteLine("Nhap sai!!!");
                            break;
                    }

                }
                catch (Exception)
                {

                    Console.WriteLine("Nhap sai!!!");
                }

            } while (true);
        }
        static  List<ClassRoom> ImportJson()        // doc file json
        {
            var content = System.IO.File.ReadAllText(@"json1.json");
            List<ClassRoom> classRooms = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ClassRoom>>(content);
            Console.WriteLine("Length: " + classRooms.Count);
            Display(classRooms);
            return classRooms;
        }    
        static  void Input(List<ClassRoom> classRooms)  //them n sinh vien
        {
            try
            {
                Console.WriteLine("Nhap so sinh vien can them:");
                int n = int.Parse(Console.ReadLine());

                for (int i = 0; i < n; i++)
                {
                    ClassRoom cls = new ClassRoom();
                    cls.Input();
                    classRooms.Add(cls);
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Nhap sai!!!!");

            }
        }
        static void AddStudent(List<ClassRoom> classRooms)  //them 1 sinh vien
        {
                ClassRoom cls = new ClassRoom();
                cls.Input1();
                classRooms.Add(cls);
         
        }
        static void Rank(List<ClassRoom> classRooms)    // xep loai sinh vien
        {
            foreach (ClassRoom classroom in classRooms)
            {
                foreach (var item in classroom.StudentList)
                {
                    if (item.Avg < 5)
                    {
                        Console.WriteLine("Sinh vien yeu!");
                        item.Display();
                    }
                    if (item.Avg >= 5 && item.Avg <7)
                    {
                        Console.WriteLine("Sinh vien Kha!");
                        item.Display();
                    }
                    if (item.Avg >= 7 && item.Avg <9)
                    {
                        Console.WriteLine("Sinh vien gioi!");
                        item.Display();
                    }
                    if (item.Avg >= 9 && item.Avg <= 10)
                    {
                        Console.WriteLine("Sinh vien xuat sac!");
                        item.Display();
                    }
                }
            }
        }
        static void WeakStudent(List<ClassRoom> classRooms) // xuat ra sinh vien diem tb <5
        {
            foreach (ClassRoom classroom in classRooms)
            {
                foreach (var item in classroom.StudentList)
                {
                    if (item.Avg<5)
                    {
                        item.Display();
                    }
                }
            }
        }
        static void FindStudent(List<ClassRoom> classRooms) // Tiem kiem tuyet doi theo ho va ten
        {
            try
            {
                Console.WriteLine("Nhap ten sinh vien can tim kiem");
                string find = Console.ReadLine();
                int a = 0;
                foreach (ClassRoom classroom in classRooms)
                {
                    foreach (var item in classroom.StudentList)
                    {
                        if (find == item.Fullname)
                        {
                            item.Display();
                            a++;
                        }
                    }
                }
                if (a == 0)
                {
                    Console.WriteLine("Khong tim thay sinh vien!");
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Nhap sai!");
                throw;
            }
            
        }
        static void FindStudent1(List<ClassRoom> classRooms) // Tiem kiem tuong doi theo ho va ten
        {
           
        }
        static void UpdateStudent(List<ClassRoom> classRooms)
        {

        }
        static void Display(List<ClassRoom> classRooms)     
        {
            foreach (ClassRoom classroom in classRooms)
            {
                classroom.DisPlay();
            }
        }
        static void ShowMenu()
        {
            Console.WriteLine("1. Doc du lieu tu file data.json");
            Console.WriteLine("2. In ra thi thong tin sinh vien");
            Console.WriteLine("3. Them 1 sinh vien");
            Console.WriteLine("4. Them nhieu sinh vien");
            Console.WriteLine("5. Xep loai cac sinh vien");
            Console.WriteLine("6. In ra sinh vien yeu");
            Console.WriteLine("7. Tim kiem dua tren ten(tuyet doi)");
            Console.WriteLine("8. Tim kiem dua tren ten(tuong doi)");
            Console.WriteLine("9. Thay doi thong tin sinh vien");
            Console.WriteLine("10. Xoa sinh vien");
            Console.WriteLine("0. Thoat chuong trinh");
            Console.WriteLine("Choose: ");
        }
    }
}
